#include <stdio.h>

void generateJson(char dest[], char* comValue, char* actValue) {
	printf("[SimpleJson] generateJson : dest - %x (%d), %s\n", dest, sizeof(dest), dest);
	printf("[SimpleJson] generateJson : commandValue, %s, actionValue, %s\n", comValue, actValue);
	snprintf(dest, 256, "{\n\"%s\" : \"%s\",\n\"%s\" : \"%s\"\n}", "command", comValue, "action", actValue);
	printf("[SimpleJson] generateJson : generated, \n%s\n", dest);
}

void generateJson2(char** dest, char* comValue, char* actValue) {
	printf("[SimpleJson] generateJson : dest - %x (%d), %s\n", *dest, sizeof(*dest), *dest);
	printf("[SimpleJson] generateJson : commandValue, %s, actionValue, %s\n", comValue, actValue);
	snprintf(*dest, 256, "{\n\"%s\" : \"%s\",\n\"%s\" : \"%s\"\n}", "command", comValue, "action", actValue);
	printf("[SimpleJson] generateJson : generated, \n%s\n", *dest);
}

int main() {
	char array1[512] = {'w','h','a','t',};
	printf("[main] array1 - %x (%d)\n", array1, sizeof(array1));
	generateJson(array1, "Computer", "Awesome");
	printf("[main] result - %s\n", array1);

	char *array2 = (char *)calloc(256, sizeof(char));
	*array2 = 'w';
	*(array2 + 1) = 'h';
	*(array2 + 2) = 'a';
	*(array2 + 3) = 't';
	printf("[main] array2 - %x (%d)\n", array2, sizeof(array2));
	generateJson2(&array2, "Computer", "Awesome");
	printf("[main] result - %s\n", array2);

	memset(array2, 0, 256);
	generateJson(array2, "gen", "json");
	//generateJson(&array2, "gen", "json"); // segmentation fault
	printf("[main] ddd.... %s\n", array2); 

	return 0;
}
