#include <stdio.h>
#include <string.h>

int main() {
	char *text1 = "hello,world";
	char *text2 = "hello,world\0";

	printf("text1 : %ld, text2 : %ld", strlen(text1), strlen(text2));
	return 0;
}
