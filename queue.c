#include <stdio.h>
#include <stdlib.h>

typedef struct _element {
	char *data;
	struct _element *next;
} Element;

typedef struct _queue {
	Element *front;
	Element *rear;
} Queue;

void put(Queue *queue, char *data) {
	Element *temp = (Element *)calloc(1, sizeof(Element));
	temp->data = data;

	if (queue->front == NULL) {
		queue->front = temp;
		queue->rear = queue->front;
	} else {
		queue->rear->next = temp;
		queue->rear = temp;
	}
}

Element *get(Queue *queue) {
	Element *temp = queue->front;
	
	if (queue->front == queue->rear) {
		queue->front = NULL;
		queue->rear = NULL;
	} else {
		queue->front = queue->front->next;
	}

	return temp;
}

int main() {
	Queue queue = {NULL, NULL};

	put(&queue, "hello");
	put(&queue, ", ");
	put(&queue, "world");
	put(&queue, "!\n");
	
	Element *q = NULL;
	for (; (q = get(&queue)) != NULL; free(q)) {
		printf("%s", q->data);
	}

	return 0;
}
