#include <stdio.h>

typedef struct _abcd {
	int length;
	char abab[];
} ABC;

int main() {
	ABC test;
	test.abab[0] = '1';
	printf("%s\n", test.abab);
	return 0;
}
