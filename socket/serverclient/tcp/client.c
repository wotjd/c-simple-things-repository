#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

const char *request = 
"SETUP rtsp://192.168.0.1:554/301.mpg?someparam\r\n"
"CSeq: 0\r\n"
"Range: npt=now-\r\n"
"Transport: RTP/AVP; unicat; destination=192.168.0.100;\r\n"
"client_port=9898-9899\r\n"
"X-pat\r\n"
"X-pmt\r\n"
"\r\n"
;

int main() {
	int socketFd;
	struct sockaddr_in address;
	char buffer[1024] = {0, };

	socketFd = socket(PF_INET, SOCK_STREAM, 0);
	if (socketFd == -1) {
		printf("failed to create socket\n");
	} else {
		memset(&address, 0, sizeof(address));
		address.sin_family = AF_INET;
		address.sin_port = htons(7000);
		address.sin_addr.s_addr = inet_addr("127.0.0.1");

		if (connect(socketFd, (struct sockaddr *)&address, sizeof(address)) == -1) {
			printf("failed to connect to server\n");
		} else {
			sprintf(buffer, request);
			write(socketFd, buffer, strlen(buffer));
			memset(buffer, 0, sizeof(buffer));
			read(socketFd, buffer, sizeof(buffer));
			printf("read from server : %s\n", buffer);
			close(socketFd);
		}
	}

	return 0;
}
