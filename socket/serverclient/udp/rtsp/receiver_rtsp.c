#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

const char *optReq =
"OPTIONS rtsp://184.72.239.149:554/vod/mp4:BigBuckBunny_175k.mov RTSP/1.0\r\n"
"CSeq: 1\r\n"
"\r\n";

const char *descReq = 
"DESCRIBE rtsp://184.72.239.149:554/vod/mp4:BigBuckBunny_175k.mov RTSP/1.0\r\n"
"CSeq: 2\r\n"
"Accept: application/sdp\r\n"
"\r\n";

const char *setReq = 
"SETUP rtsp://184.72.239.149:554/vod/mp4:BigBuckBunny_175k.mov RTSP/1.0\r\n"
"CSeq: 3\r\n"
"Transport: RTP/AVP;unicast;interleaved=0-1\r\n"
"\r\n";

const char *playReq = 
"PLAY rtsp://184.72.239.149:554/vod/mp4:BigBuckBunny_175k.mov/ RTSP/1.0\r\n"
""
;

char *getNextLine(char *data) {
	char *result = NULL;
	result = strchr(data, '\n') + 1;
	return result;
}

void parseRtst(const char *msg) {

}

int main() {
	int rtspSock, rtpSock;

	struct sockaddr_in rtspInfo, rtpInfo;

	char buffer[1024] = {0, };

	rtspSock = socket(PF_INET, SOCK_STREAM, 0);
	rtpSock = socket(PF_INET, SOCK_DGRAM, 0);

	if (rtspSock == -1 || rtpSock == -1) {
		printf("failed to create rtspSock\n");
	} else {
		memset(&rtspInfo, 0, sizeof(rtspInfo));
		memset(&rtpInfo, 0, sizeof(rtpInfo));

		rtspInfo.sin_family = AF_INET;
		rtspInfo.sin_addr.s_addr = inet_addr("184.72.239.149");
		rtspInfo.sin_port = htons(554);


		rtpInfo.sin_family = AF_INET;
		rtpInfo.sin_addr.s_addr = inet_addr("184.72.239.149");
		rtpInfo.sin_port = htons(554);

		int reuseflag = 1;
		if ((setsockopt(rtspSock, SOL_SOCKET, SO_REUSEADDR, &reuseflag, sizeof(reuseflag)) < 0) && (setsockopt(rtpSock, SOL_SOCKET, SO_REUSEADDR, &reuseflag, sizeof(reuseflag)) < 0)) {
			printf("cannot set socket to reuse\n");
		}
		if (connect(rtspSock, (struct sockaddr *)&rtspInfo, sizeof(rtspInfo)) == -1) {
			// printf("failed to bind socket\n");
			perror("bind");
		} else {
			sprintf(buffer, optReq);
			if (write(rtspSock, buffer, strlen(buffer)) != strlen(buffer)) {
				printf("failed to send\n");
			} else {
				memset(buffer, 0, sizeof(buffer));
				printf("sended\n");
				read(rtspSock, buffer, sizeof(buffer));
				printf("buffer\n===============\n%s\n", buffer);

				memset(buffer, 0, sizeof(buffer));
				sprintf(buffer, descReq);
				if (write(rtspSock, buffer, strlen(buffer)) != strlen(buffer)) {
					printf("failed to send\n");
				} else {
					memset(buffer, 0, sizeof(buffer));
					printf("sended\n");
					read(rtspSock, buffer, sizeof(buffer));
					printf("buffer\n===============\n%s\n", buffer);

					memset(buffer, 0, sizeof(buffer));
					sprintf(buffer, setReq);
					if (write(rtspSock, buffer, strlen(buffer)) != strlen(buffer)) {
						printf("failed to send\n");
					} else {
						memset(buffer, 0, sizeof(buffer));
						printf("sended\n");
						read(rtspSock, buffer, sizeof(buffer));
						printf("buffer\n===============\n%s\n", buffer);
						

						/*
						if (bind(rtpSock, (struct sockaddr *)&rtspInfo, sizeof(rtspInfo)) < 0) {
							perror("bind");
						} else {
							printf("bind done\n");
						}
						*/
					}
				}
			}
		}
		close(rtspSock);
		close(rtpSock);
	}

	return 0;
}
