#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

int main() {
	int socketFd;
	struct sockaddr_in address;
	char buffer[1024] = {0, };
	request = "hello, world!\n";

	socketFd = socket(PF_INET, SOCK_DGRAM, 0);
	if (socketFd == -1) {
		printf("failed to create socket\n");
	} else {
		memset(&address, 0, sizeof(address));
		address.sin_family = AF_INET;
		address.sin_port = htons(7500);
		address.sin_addr.s_addr = inet_addr("225.222.112.44");
		int addrlen = sizeof(address);
		int ttl = 64;
		if (setsockopt(socketFd, IPPROTO_IP, IP_MULTICAST_TTL, (void *)&ttl, sizeof(ttl)) < 0) {
			printf("cannot set multicast ttl\n");
		} else {
			sprintf(buffer, request);
			while(1) {
				if (sendto(socketFd, buffer, strlen(buffer), 0, (struct sockaddr *)&address, sizeof(address)) != strlen(buffer)) {
					printf("failed to send\n");
				} else {
					printf("sended\n");
					usleep(3000 * 1000);
				}
			}
			close(socketFd);
		}
	}

	return 0;
}
