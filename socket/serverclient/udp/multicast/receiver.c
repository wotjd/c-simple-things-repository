#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>


int main() {
	int sockFd;

	struct sockaddr_in addressInfo;

	char buffer[1024] = {0, };

	sockFd = socket(PF_INET, SOCK_DGRAM, 0);
	if (sockFd == -1) {
		printf("failed to create sockFd\n");
	} else {
		memset(&addressInfo, 0, sizeof(addressInfo));
		addressInfo.sin_family = AF_INET;
		// addressInfo.sin_addr.s_addr = inet_addr("233.18.158.160");
		// addressInfo.sin_port = htons(5000);
		addressInfo.sin_port = htons(7500);
		addressInfo.sin_addr.s_addr = inet_addr("225.222.112.44");

		// SHOULD BE RUNNED BEFORE CALLING bind()
		int reuseflag = 1;
		if (setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, &reuseflag, sizeof(reuseflag)) < 0) {
			printf("cannot set socket to reuse\n");
		}

		if (bind(sockFd, (struct sockaddr *)&addressInfo, sizeof(addressInfo)) == -1) {
			printf("failed to bind socket\n");
		} else {
			struct ip_mreq mreq;

			/* Join the Multicast Group */
			printf("Joining IPv4 Multicast Address Group\n");
			mreq.imr_multiaddr.s_addr = addressInfo.sin_addr.s_addr;
			mreq.imr_interface.s_addr = htonl(INADDR_ANY);
			if (setsockopt(sockFd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq, sizeof(mreq)) < 0) {
				printf("cannod add membership");
			} else {
				int reuseflag = 1;
				if (setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, &reuseflag, sizeof(reuseflag)) < 0) {
					printf("cannot set socket to reuse\n");
				}
				struct sockaddr_in clientInfo;
				int clientLen = sizeof(clientInfo);
				while(1) {
					int readn = 0;
					memset(buffer, 0, sizeof(buffer));
					// save client info
					if ((readn = recvfrom(sockFd, buffer, sizeof(buffer), 0, (struct sockaddr*)&clientInfo, &clientLen)) < 0) {
						printf("read fail\n");
						break;
					}
					printf("multicast message : %s\n", buffer);
				}
			}
			close(sockFd);
		}
	}

	return 0;
}
