#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>


int main() {
	int sockFd;

	struct sockaddr_in addressInfo;

	char buffer[1024] = {0, };

	sockFd = socket(PF_INET, SOCK_DGRAM, 0);
	if (sockFd == -1) {
		printf("failed to create sockFd\n");
	} else {
		memset(&addressInfo, 0, sizeof(addressInfo));
		addressInfo.sin_family = AF_INET;
		addressInfo.sin_port = htons(7500);
		addressInfo.sin_addr.s_addr = htonl(INADDR_ANY);

		if (bind(sockFd, (struct sockaddr *)&addressInfo, sizeof(addressInfo)) == -1) {
			printf("failed to bind socket\n");
		} else {
			struct sockaddr_in clientInfo;
			int clientLen = sizeof(clientInfo);
			while(1) {
				int readn = 0;
				memset(buffer, 0, sizeof(buffer));
				// save client info
				if ((readn = recvfrom(sockFd, buffer, sizeof(buffer), 0, (struct sockaddr*)&clientInfo, &clientLen)) < 0) {
					printf("read fail\n");
					break;
				} else if (sendto(sockFd, buffer, readn, 0, (struct sockaddr*)&clientInfo, sizeof(clientInfo)) != readn) {
					printf("send fail\n");
					break;
				}
				printf("echo message : %s\n", buffer);
			}
			close(sockFd);
		}
	}

	return 0;
}
