#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

int main() {
	struct addrinfo* addressList;
	
	// resolve the domain name into a list of address
	if (getaddrinfo("www.naver.com", NULL, NULL, &addressList)) {
		printf("getaddrinfo error\n");
		return 0;
	} else {
		struct addrinfo* addressIndex;
		// loop over all returned results and do inverse lookup
		for (addressIndex = addressList; addressIndex; addressIndex = addressIndex->ai_next) {
			char hostname[1025] = "";

			if (getnameinfo(addressIndex->ai_addr, addressIndex->ai_addrlen, hostname, 1025, NULL, 0, 0)) {
				printf("getnameinfo error");
			} else if (hostname[0] != '\0') {
				printf("hostname: %s\n", hostname);
			} else {
				printf("hostname is null");
			}
		}

		freeaddrinfo(addressList);
	}
	return 1;
}
