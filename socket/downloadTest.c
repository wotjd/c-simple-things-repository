#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

// very very simple image donwloader

static ssize_t readn(int fd, void *ptr, size_t n) {
	size_t nleft = n;

	while (nleft > 0) {
		ssize_t nread = 0;
		if ((nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR) {
				nread = 0;	// call read again
			} else {
				return -1;
			}
		} else if (nread == 0) {
			break;
		}

		nleft -= nread;
		ptr += nread;
	}
	return (n - nleft);
}

static ssize_t writen(int fd, const void *ptr, size_t n) {
	size_t nleft = n;

	while (nleft > 0) {
		ssize_t nwritten;
		if ((nwritten = write(fd, ptr, nleft)) < 1) {
			if (nwritten < 0 && errno == EINTR) {
				nwritten = 0;	// call write again
			} else {
				return -1;
			}
		}

		nleft -= nwritten;
		ptr += nwritten;
	}

	return n;
}

unsigned char* downloadImage(const char *ip, short port, char *route, int *size) {
	int socketFd;
	struct sockaddr_in address;

	socketFd = socket(PF_INET, SOCK_STREAM, 0);

	if (socketFd == -1) {
		printf("[ImageDownloader] %s : failed to create socket\n", __FUNCTION__);
	} else {
		memset(&address, 0, sizeof(address));
		address.sin_family = AF_INET;
		address.sin_port = htons(port);
		address.sin_addr.s_addr = inet_addr(ip);
		printf("h1\n");
		if (connect(socketFd, (struct sockaddr *)&address, sizeof(address)) == -1) {
			printf("[ImageDownloader] %s : failed to connect to server\n", __FUNCTION__);
		} else {
			printf("hi\n");
			char buffer[4096] = {0, };		// apache's max http header buffer is 4K
			char *header = buffer;
			sprintf(header + strlen(header), "GET /%s ", route);
			sprintf(header + strlen(header), "HTTP/1.1\r\n");
			sprintf(header + strlen(header), "Host: %s:%d\r\n", ip, port);
			sprintf(header + strlen(header), "Connection: keep-alive\r\n");
			sprintf(header + strlen(header), "Accept: */*\r\n");
			sprintf(header + strlen(header), "Cache-Control: no-cache\r\n");
			sprintf(header + strlen(header), "User-Agent: Castanets HTTP v1.1\r\n\r\n");

			printf("[ImageDownloader] %s : header to send - \n%s\n", __FUNCTION__, buffer);

			write(socketFd, buffer, strlen(buffer));
			memset(buffer, 0, sizeof(buffer));
			read(socketFd, buffer, sizeof(buffer));
			printf("[ImageDownloader] %s : read header from server - \n%s\n", __FUNCTION__, buffer);

			char *contentLength = strstr(buffer, "Content-Length");
			if (contentLength != NULL) {
				int bodySize = 0;
				sscanf(contentLength, "%*[^:]: %d\n", &bodySize);
				printf("[ImageDownloader] %s : content length - %d\n", __FUNCTION__, bodySize);

				unsigned char *bodyData = (unsigned char *)calloc(bodySize + 1, sizeof(char));
				if (bodyData != NULL) {
					if (readn(socketFd, bodyData, bodySize) < 0) {
						printf("[ImageDownloader] %s : failed to download image\n", __FUNCTION__);
						free(bodyData);
					} else {
						printf("[ImageDonwloader] %s : donwloaded image successfully\n");
						if (size != NULL) {
							*size = bodySize;
						}
						return bodyData;
					}

				} else {
					printf("[ImageDownloader] %s : failed to alloc\n", __FUNCTION__);
				}
			} else {
				printf("[ImageDownloader] %s : failed to find content length in header\n", __FUNCTION__);
			}
		}
		return NULL;
	}
}

void hostname_to_ip(char *hostname, char *ip) {
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_in *h;
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(hostname, "http", &hints, &servinfo)) == 0) {
		for (p = servinfo; p != NULL; p = p->ai_next) {
			h = (struct sockaddr_in *)p->ai_addr;
			strcpy(ip, inet_ntoa(h->sin_addr));
		}
		freeaddrinfo(servinfo);
	}
}

int main() {
	int size;
	//char *hostname = "vignette3.wikia.nocookie.net";
	
	char ip[100] = "52.79.189.124";
	//hostname_to_ip(hostname, ip);	
	downloadImage(ip, 8080, "imageTest/poster.png", &size);
	printf("size : %d\n", size);
	return 0;
}
