#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

int main() {
	int listenFd, connectFd;

	struct sockaddr_in addressInfo;

	char buffer[1024] = {0, };

	listenFd = socket(PF_INET, SOCK_STREAM, 0);
	if (listenFd == -1) {
		printf("failed to create listenFd\n");
	} else {
		memset(&addressInfo, 0, sizeof(addressInfo));
		addressInfo.sin_family = AF_INET;
		addressInfo.sin_port = htons(7000);
		addressInfo.sin_addr.s_addr = htonl(INADDR_ANY);

		if (bind(listenFd, (struct sockaddr *)&addressInfo, sizeof(addressInfo)) == -1) {
			printf("failed to bind socket\n");
		} else {
			if (listen(listenFd, 5) == -1) {
				printf("failed to listen\n");
			} else {
				while (1) {
					connectFd = accept(listenFd, (struct sockaddr *)NULL, NULL);
					if (connectFd == -1) {
						printf("failed to connect\n");
					} else {
						char temp[1024] = {0, };
						read(connectFd, buffer, 1024);
						printf("read from client : %s", buffer);
						sprintf(temp, "string you sent : %s", buffer);
						write(connectFd, temp, strlen(temp));
						close(connectFd);
					}
				}
			}
		}
	}

	return 0;
}
