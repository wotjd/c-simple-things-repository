#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#include <arpa/inet.h>

void hostname_to_ip(char *, char *);

int main() {
	// char *hostname = "www.naver.com";
	char *hostname = "images2.fanpop.com";
	char ip[100];
	 
	hostname_to_ip(hostname, ip);
	printf("%s resolved to %s\n", hostname, ip);
	return 0;
}

void hostname_to_ip(char *hostname, char *ip) {

	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_in *h;
	int rv;
	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(hostname, "http", &hints, &servinfo)) != 0) {
		printf("error\n");
	} else {
		for (p = servinfo; p != NULL; p = p->ai_next) {
			h = (struct sockaddr_int *)p->ai_addr;
			strcpy(ip, inet_ntoa(h->sin_addr));
		}
		freeaddrinfo(servinfo);
	}
}
