#include <stdio.h>
#include <string.h>

int main() {
	const char *data = "HTTP/1.1 200 OK\r\n"
		"X-Powered-By: Express\r\n"
		"Accept-Ranges: bytes\r\n"
		"Cache-Control: public, max-age=0\r\n"
		"Last-Modified: Fri, 07 Apr 2017 01:53:55 GMT\r\n"
		"ETag: W/\"ac81f-15b461e2fef\"\r\n"
		"Content-Type: image/png\r\n"
		"Content-Length: 706591\r\n"
		"Date: Wed, 28 Jun 2017 05:40:37 GMT\r\n"
		"Connection: keep-alive\r\n\r\n";

	char *found = strstr(data, "Content-Length");

	if (found) {
		int cLength = 0;
		printf("found string : \n%s\n", found);
		sscanf(found, "%*[^:]: %d", &cLength);
		printf("Content-Length : %d\n", cLength);
	}

//	printf("delimited : %s\n", buffer);
//	printf("parsed : %d\n", cLength);
	return 0;
}
