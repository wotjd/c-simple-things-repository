#include <stdio.h>
#include <stdbool.h>

bool loadChannelList(){
	FILE *file = fopen("/home/wotjd/temp/channelList.txt", "r");
	bool isEof = false;
	if (file != NULL) {
		int line = 0;
		while (!isEof) {
			char temp[1024] = {0,};
			line++;
			if (fgets(temp, sizeof(temp), file) != NULL) {
				if (temp[0] == '#') {
					printf("[loadChannelList] passing comment line\n");
				} else {
					char name[256] = {0,}, ip[16] = {0,}, port[6] = {0,}, dumpName[256] = {0, }, dumpIp[16] = {0,};
					short vpid = 0, apid = 0;
					int vtype = 0, atype = 0;
					printf("str : %s", temp);
		
					if (sscanf(temp, "%[^,], %[^,], %[^,], %[^,], %[^,], %hd, 0x%x, %hd, 0x%x", name, ip, port, dumpName, dumpIp, &vpid, &vtype, &apid, &atype) != 9) {
						printf("[loadChannelList] Syntax error, at line : %d\n", line);
						break;
					} else {
						printf("[loadChannelList] read name : %s, ip : %s, port : %s, dumpname : %s, dumpip : %s, vpid : %d, vtype : 0x%x, apid : %d, atype : 0x%x\n",
							name, ip, port, dumpName, dumpIp, vpid, vtype, apid, atype);
					}
				}
			} else {
				printf("[loadChannelList] reached eof\n");
				isEof = true;
			}
		}
		fclose(file);
	} else {
		printf("[loadChannelList] file is NULL!\n");
	}

	return isEof;
}

int main() {
	printf("loading channel list : %s\n", loadChannelList()? "success": "fail");
}
