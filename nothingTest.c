#include <stdio.h>

#ifdef DEBUGGING_LOG_ON
	#define dp(...) printf(__VA_ARGS__)
	#define dlp(fmt,args...) printf( "[%s %d]" fmt, __FILE__,__LINE__, ## args )
#else
	#define dp(...) 
	#define dlp(...) 
#endif

int	main() {
	printf("before\n");
	dp("debugging log\n");
	printf("after\n");
	return 1;
}
