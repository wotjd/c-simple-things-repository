#include <stdio.h>

void constTest(const char* string) {
	printf("[constTest] string : %s, %x\n", string, string);
}

int main() {
	char *string = "hello, world";

	printf("[main] string : %s, %x\n", string, string);
	constTest(string);

	return 0;
}
