#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "KeySender.h"

static int socketFd;
struct sockaddr_in address;

bool connectToKeyServer(char *ip, short port) {
	socketFd = socket(PF_INET, SOCK_STREAM, 0);
	if (socketFd == -1) {
		printf("failed to create socket\n");
	} else {
		memset(&address, 0, sizeof(address));
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = inet_addr(ip);
		address.sin_port = htons(port);
		printf("hi\n");
		if (connect(socketFd, (struct sockaddr *)&address, sizeof(address)) == -1) {
			printf("failed to connect to server\n");
		} else {
			printf("connected\n");
			return true;
		}
	}
}

static bool writen(int fd, void *ptr, int n) {
	int nleft = n;
	int nwritten = 0;
	while (nleft > 0) {
		if ((nwritten = write(fd, ptr, nleft)) <= 0) {
			if (nwritten < 0 && errno == EINTR) {
				nwritten = 0;
			} else {
				nwritten = -1;
			}
		}
		nleft -= nwritten;
		ptr += nwritten;
	}
	return (nleft == 0);
}

static int convertKeyCode(int keyCode) {
	int converted = keyCode;
	return converted;
}

bool sendKey(int keyCode) {
	keyCode = htonl(convertKeyCode(keyCode));
	printf("hi\n");
	return writen(socketFd, (void *)&keyCode, 4);
}

int main() {
	connectToKeyServer("172.16.81.40", 7777);
	while((getchar() != 'e')) {
		printf("result : %s\n", (sendKey(2))? "true": "false");
	}
	return 0;
}
