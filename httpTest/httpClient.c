#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

ssize_t readn(int fd, void *ptr, size_t n) {
	size_t nleft = n;

	while (nleft > 0) {
		ssize_t nread = 0;
		if ((nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR) {
				nread = 0;	// call read again
			} else {
				return -1;
			}
		} else if (nread == 0) {
			break;
		}

		nleft -= nread;
		ptr += nread;
	}
	return (n - nleft);
}

ssize_t writen(int fd, const void *ptr, size_t n) {
	size_t nleft = n;

	while (nleft > 0) {
		ssize_t nwritten;
		if ((nwritten = write(fd, ptr, nleft)) <= 0) {
			if (nwritten < 0 && errno == EINTR) {
				nwritten = 0;	// and call write again
			} else {
				return -1;
			}
		}

		nleft -= nwritten;
		ptr += nwritten;
	}

	return n;
}

void saveImageFile(char* data, size_t length) {
	FILE* file;
	if ((file = fopen("./saved.png", "w")) == NULL) {
		printf("failed to open file\n");
	} else {
		printf("file size : %ld\n", length);
		fwrite(data, 1, length, file);
		fclose(file);
	}
}

int main() {
	int socketFd;
	struct sockaddr_in address;

	int port = 8080;
	char *ip = "52.79.189.124";

	socketFd = socket(PF_INET, SOCK_STREAM, 0);
	if (socketFd == -1) {
		printf("failed to create socket\n");
	} else {
		memset(&address, 0, sizeof(address));
		address.sin_family = AF_INET;
		address.sin_port = htons(port);
		address.sin_addr.s_addr = inet_addr(ip);

		if (connect(socketFd, (struct sockaddr *)&address, sizeof(address)) == -1) {
			printf("failed to connect to server\n");
		} else {
			char buffer[1024] = {0, };

			sprintf(buffer + strlen(buffer), "GET /imageTest/poster.png ");
			sprintf(buffer + strlen(buffer), "HTTP/1.1\r\n");
			sprintf(buffer + strlen(buffer), "Host: %s:%d\r\n", ip, port);
			sprintf(buffer + strlen(buffer), "Connection: keep-alive\r\n");
			sprintf(buffer + strlen(buffer), "Accept: */*\r\n");
			sprintf(buffer + strlen(buffer), "Cache-Control: no-cache\r\n");
			sprintf(buffer + strlen(buffer), "User-Agent: Custom httpClient\r\n\r\n");

			printf("header to send : \n%s\n", buffer);
			write(socketFd, buffer, strlen(buffer));
			memset(buffer, 0, sizeof(buffer));
			int nread = read(socketFd, buffer, sizeof(buffer));
			printf("read from server : \n%s\n", buffer);
	
			char *found = strstr(buffer, "Content-Length");

			if (found) {
				size_t contentLength = 0;
				sscanf(found, "%*[^:]: %ld", &contentLength);
				printf("parsed content length : %ld\n", contentLength);

				char *bodyData = (char *)calloc(contentLength + 1, sizeof(char));	//add NULL
				if (bodyData) {
					char *bodyends = NULL;
					int lnread = nread, bodyread = 0;
					while ((bodyends = strstr(buffer, "\r\n\r\n")) == NULL) {
						memset(buffer, 0, 1024);
						nread += lnread = read(socketFd, buffer, sizeof(buffer));
					}
					bodyends += 4;
					bodyread = buffer + lnread - bodyends;
					printf("nread : %d, last nread : %d\n", nread, lnread);
					printf("\\r\\n\\r\\n : %x\n", bodyends);
					printf("read body %d bytes\n", bodyread);
					printf("body start : %s\n", bodyends);
					printf("body to read : %d bytes\n", contentLength - bodyread);
					
					memcpy(bodyData, bodyends, bodyread);

					readn(socketFd, bodyData + bodyread, contentLength - bodyread);
					saveImageFile(bodyData, contentLength);
						
					free(bodyData);
				}
				
//				printf("bodyData : \n%s\n", bodyData);
			}

			close(socketFd);
		}
	}

	return 0;
}
