#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <stdbool.h>

static bool ci_netutil_readn_async_http_custom(int sock, void *Headerbuf, char **Bodybuf, int iTimeOutInSec, int *iContentLength) {
	int nleft = 1;
	char arrayReceivedPart[256+1];

	int nReceived_byte = 0;
	int nTotalReceived_byte = 0;
	int nBodyRead = 0;
	int iTimeOutCount = 0;

	char *ptr;
	char *ptrTemp;

	int iHeaderSize = 0;
	int iBodySize = 0;
	char *pBody;
	char *tmp;

	int iCurrTimeoutCount = 0;
	int iTimeoutInMilliSec = iTimeOutInSec*1000;

	ptr = (char *)Headerbuf;
	ci_mem_set(arrayReceivedPart, 0, 256+1);

	while ( nleft > 0 ) {
		nReceived_byte = ci_recv(sock, arrayReceivedPart, nleft);
		if ( nReceived_byte < 0 ) {
			int iError = ci_sock_get_last_error();
			if ( iError == CI_EWOULDBLOCK ) {
				iTimeOutCount++;
				iCurrTimeoutCount = 10 * iTimeOutCount;
				if ( iCurrTimeoutCount > iTimeoutInMilliSec ) {
					return 0;
				}

				ci_thread_sleep(10);
				continue;
			}
			else {
				return 0;
			}
		}
		else if (nReceived_byte == 0) {
			break;	/* EOF */
		}
		else if (nReceived_byte > 0) {
			ci_mem_cpy(ptr+nTotalReceived_byte, arrayReceivedPart, nReceived_byte);
			nTotalReceived_byte += nReceived_byte;
			
			// Find success "\r\n\r\n"
			if ( (pBody = strstr(ptr, "\r\n\r\n")) != NULL ) {
				ptrTemp = strstr(ptr, "Content-Length:");
				if ( ptrTemp == NULL ) {
					// there is only header
					return 1;
				} else {
					iHeaderSize = nTotalReceived_byte - strlen(pBody) + 4/*CRLFCRLF*/;
					
					if (sscanf(ptrTemp, "Content-Length: %d", &iBodySize) < 0) {
						// there is some parsing error
						return 0;
					}

					if ( nTotalReceived_byte >= iHeaderSize + iBodySize ) {
						// already received header & body
						tmp = (char*)ci_mem_alloc(iBodySize+1);
						ci_mem_set(tmp, '\0', iBodySize + 1);
						memcpy(tmp, pBody+4, iBodySize );
						*Bodybuf = tmp;
						return 1;
					}

					tmp = (char*)ci_mem_alloc(iBodySize + 1);
					ci_mem_set(tmp, '\0', iBodySize + 1);
					memcpy(tmp, pBody+4, nTotalReceived_byte - iHeaderSize);

					if ( ci_netutil_readn_async(sock,tmp+(nTotalReceived_byte - iHeaderSize),iBodySize - (nTotalReceived_byte-iHeaderSize) ,&nBodyRead,iTimeOutInSec) == 1 ) {
						if (iBodySize - (nTotalReceived_byte-iHeaderSize) == nBodyRead) {
							//received remains Body successed
							*Bodybuf = tmp;
							*iContentLength = iBodySize;
							return 1;
						} else {
							//fail to received body
							if ( tmp != NULL ) ci_mem_free(tmp);
							return 0;
						}
					} else {				
						// there is some error receving Body
						if ( tmp != NULL ) ci_mem_free(tmp);
						return 0;
					}
				}
				return 1;
			} else {
				// 256 more read
				nleft = 1;
			}
		}
		iTimeOutCount = 0;
	}
	return 0;
}