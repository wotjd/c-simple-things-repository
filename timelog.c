#include <stdio.h>
#include <time.h>

int main() {
	time_t now = time(0);

	printf("%s", ctime(&now));
	return 1;
}
