#ifndef __STOP_WATCH_H__
#define __STOP_WATCH_H__

#include <stdbool.h>

bool start();
double getTime();
void end();

#endif
