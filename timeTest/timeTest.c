#include <stdio.h>
#include <sys/time.h>

typedef struct timeval timeObj;

double getOperatingTime(timeObj *start, timeObj *end) {
	return (double)(end->tv_sec) + (double)(end->tv_usec)/1000000.0 - (double)(start->tv_sec) - (double)(start->tv_usec)/1000000.0;
}

int main() {

	timeObj start, end;
	double opTime;
	printf("enter to start\n");
	getchar();
	gettimeofday(&start, NULL);
	printf("started time operation\n");
	getchar();
	gettimeofday(&end, NULL);

//	opTime = (double)(end.tv_sec) + (double)(end.tv_usec)/1000000.0 - (double)(start.tv_sec)-(double)(start.tv_usec)/1000000.0;

	
	printf("operating time : %lf\n", getOperatingTime(&start, &end));
	return 0;
}
