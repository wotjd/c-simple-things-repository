#include <stdio.h>
#include "StopWatch.h"

int main() {
	start();
	while (getchar() != 'e') {
		printf("time : %lf\n", getTime());
	}
	end();
	return 0;
}
