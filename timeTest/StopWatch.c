#include <stdio.h>
#include <sys/time.h>
#include "StopWatch.h"

typedef struct timeval TimeObj;

static TimeObj s, e;
static bool isStarted = false;

static double calculateTime() {
	return (double)(e.tv_sec) + (double)(e.tv_usec)/1000000.0 - (double)(s.tv_sec) - (double)(s.tv_usec)/1000000.0;
}

bool start() {
	bool result = !isStarted;

	if (!isStarted) {
		gettimeofday(&s, NULL);
		isStarted = true;
	}

	return result;
}

double getTime() {
	double result = 0.0;
	
	if (isStarted) {
		gettimeofday(&e, NULL);
		result = calculateTime();
	}

	return result;
}

void end() {
	isStarted = false;
}
