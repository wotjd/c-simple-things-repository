#include <stdio.h>

void makeArray (int size) {
	printf("making array... size : %d\n", size);
	char array1[size];
	printf("made array... size : %d\n", sizeof(array1));
}

int main() {
	int i = 0;
	printf("enter size of array : ");
	scanf("%d", &i);
	printf("you entered %d\n", i);
	makeArray(i);
}
