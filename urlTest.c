#include <stdio.h>
#include <string.h>

int main() {
	const char *url = "http://110.35.173.36:1554/baskin.mpg/fortest";
	
	if (!strncmp(url, "http://", 7)) {
		char ip[16] = {0, };
		unsigned short port = 0;
		char route[36] = {0, };

		printf("%d\n", sscanf(url, "http://%[^:]: %hd %s", ip, &port, route));
		printf("%s, %d, %s\n", ip, port, route);
	}

	return 0;
}
