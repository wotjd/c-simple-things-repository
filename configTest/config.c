#include <stdio.h>
#include <string.h>

char ipList[100][15], portList[100][5];
short vpidList[100], apidList[100];
unsigned char vtypeList[100], atypeList[100];

int main(){
	FILE *file = fopen("./file.txt", "r");
	if(file != NULL){
		int line = 0;
		while(!feof(file)){
			char temp[1024] = {0,};
			if(fgets(temp, sizeof(temp), file) != NULL){
				switch(temp[0]){
					case '#' : break;
					default : {
						char ip[15] = {0,}, port[5] = {0,};
						short vpid = 0, apid = 0;
						int vtype = 0, atype = 0;
						printf("str : %s", temp);
						if(sscanf(temp, "%[^,], %[^,], %hd, 0x%x, %hd, 0x%x", ip, port, &vpid, &vtype, &apid, &atype) != 6){
							printf("Syntax error, at line %d\n", line);
							return 1;
						} else {
							printf("ip : %s, port : %s, vpid : %d, vtype : 0x%x, apid : %d, atype : 0x%x\n",
									ip, port, vpid, vtype, apid, atype);
						 	strncpy(ipList[line], ip, 15);
							strncpy(portList[line], port, 5);
							vpidList[line] = vpid;
							apidList[line] = apid;
							vtypeList[line] = vtype;
							atypeList[line] = atype;
							printf("ip : %s, port : %s, vpid : %d, vtype : 0x%x, apid : %d, atype : 0x%x\n",
									ipList[line], portList[line], vpidList[line], vtypeList[line], apidList[line], atypeList[line]);
							line++;
						}

					}
				}
			}
		}
		fclose(file);
	} else {
		printf("file is NULL!\n");
	}
	int i = 0;
	for(; i < 3; i++)
		printf("%d. %s\n", i, ipList[i]);

	return 0;
}
