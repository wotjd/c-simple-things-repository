#include <stdio.h>
#include <stdbool.h>
#include <string.h>

char channelIP[3][15], channelPort[3][5];
short channelVideoPid[3], channelAudioPid[3];
unsigned char channelVideoType[3], channelAudioType[3];

bool loadChannelList(){
  FILE *file = fopen("./channelList.txt", "r");
  if(file != NULL){
    int line = 0, channelCount = 0;
    while(!feof(file)){
      char temp[1024] = {0,};
      line++;
      if(fgets(temp, sizeof(temp), file) != NULL){
        switch(temp[0]){
          case '#' : break;
          default : {
            char ip[15] = {0,}, port[5] = {0,};
            short vpid = 0, apid = 0;
            int vtype = 0, atype = 0;
            printf("str : %s", temp);
            if(sscanf(temp, "%[^,], %[^,], %hd, 0x%x, %hd, 0x%x", ip, port, &vpid, &vtype, &apid, &atype) != 6){
              printf("Syntax error, at line : %d", line);
              return 1;
            } else {
              printf("read ip : %s, port : %s, vpid : %d, vtype : 0x%x, apid : %d, atype : 0x%x\n",
                ip, port, vpid, vtype, apid, atype);
              strncpy(channelIP[channelCount], ip, 15);
              strncpy(channelPort[channelCount], port, 5);
              channelVideoPid[channelCount] = vpid;
              channelAudioPid[channelCount] = apid;
              channelVideoType[channelCount] = vtype;
              channelAudioType[channelCount] = atype;
              printf("saved [%d] ip : %s, port : %s, vpid : %d, vtype : 0x%x, apid : %d, atype : 0x%x\n", channelCount,
                channelIP[channelCount], channelPort[channelCount], channelVideoPid[channelCount], channelVideoType[channelCount], channelAudioPid[channelCount], channelAudioType[channelCount]);
              channelCount++;
            }
          }
        }
      }
    }
    fclose(file);
  } else {
    printf("file is NULL!\n");
    return 1;
  }
  return 0;
}

int main(){
	if(loadChannelList()){
		printf("load channel list failed.\n");
	}
}
