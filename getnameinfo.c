#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>
 
int main()
{
	struct sockaddr_in sa;
	sa.sin_family = AF_INET;
	inet_pton(AF_INET, "127.0.0.1", &sa.sin_addr);

	char node[NI_MAXHOST];
	int res = getnameinfo((struct sockaddr*)&sa, sizeof(sa), node, sizeof(node), NULL, 0, 0);
	if (res)
	{
		printf("&#37;s\n", gai_strerror(res));
		exit(1);
	}
	printf("%s\n", node);

	return 0;
}
