#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <stdio.h>

#ifdef __DEBUG_MODE
	#define Log(...) printf("%s : ", __FUNCTION__); printf(__VA_ARGS__)
#else
	#define Log(...)
#endif

#endif
