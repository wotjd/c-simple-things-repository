#include <stdio.h>

void correct(int *x) {
	*x = 5;
	*x += 2;
	*x *= 3;
}

int main() {
	int a = 0;
	correct(&a);
	printf("result : %d\n", a);
	return 0;
}
