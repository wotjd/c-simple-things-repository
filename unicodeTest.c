#include <stdio.h>
#include <wchar.h>

static wchar_t text[16] = {0,};

int main() {
	//wchar_t text[16] = {0,};
	int i = 15;
	while (i) {
		swprintf(text, 16, L"%d sec\n", i--);
		wprintf(text);
	}
	return 0;
}
