#include <stdio.h>

unsigned char reverseBit (unsigned char bit) {
	unsigned char map[16] = {0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};
	return ((map[bit & 0x0f] << 4) + (map[(bit & 0xf0) >> 4]));
}

int main () {
	char input = 0;
	while((input = getchar()) != 'q'){
		if(input != 0xA){
			printf("----- input  : 0x%X -----\n", input);
			printf("----- output : 0x%X -----\n", reverseBit(input));
		}
	}
}
