#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <netdb.h>

int main(int argc, char **argv) {
	struct hostent *host = NULL;
	struct sockaddr_in addr;
	
	if (argc > 1) {
		char *url = argv[1];
		printf("input url : %s\n", url);

		host = gethostbyname(url);
		if (host) {
			printf("ip type : IPv%d\n", host->h_addrtype == AF_INET? 4:6);
			printf("ip address\n");

			int i;
			for (i = 0; host->h_addr_list[i]; i++) {
				printf("%d - %s\n", i, inet_ntoa(*(struct in_addr *)host->h_addr_list[i]));
			}
		} else {
			printf("gethost error.\n");
		}
	} else {
		printf("argument error.\n");
	}
	return 1;
}
