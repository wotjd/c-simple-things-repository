#include <stdio.h>
#include <string.h>

char *getNextLine(char *data) {
	char *result = strchr(data, '\n');
//	result += (result != NULL)? 1 : 0;
	return result + ((result == NULL)? 0 : 1);
}

int main() {
//	const char *str = "This is a long string.\nIt has multiple lines of text in it.\nWe want to examine each of these lines separately.\nSo we will do that";
//	printf("original : %s\n", str);
//	printf("after f : %s", getNextLine(str));
	const char *str =
		"DESCRIBE rtsp://184.72.239.149:554/vod/mp4:BigBuckBunny_175k.mov RTSP/1.0\r\n"
		"CSeq: 2\r\n"
		"Accept: application/sdp\r\n"
		"\r\n";
	char *i = NULL;
	for (i = str; i != NULL; i = getNextLine(i)) {
		//printf("looping.. : %s\n", i);
		char key[2048] = {0, };
		char value[2048] = {0, };
		if (sscanf(i, "%[^:]: %[^\r]\r\n", key, value) == 2) {
			printf("%s : %s (key : value)\n", key, value);
		}
	}
}
