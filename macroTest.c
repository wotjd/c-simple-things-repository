#include <stdio.h>

#define AA
//#define BB
int main() {

#if (defined AA)
	printf("1\n");
#endif	
#if !(defined BB)
	printf("2\n");
#endif
#if !(defined BB) && (defined AA)
	printf("3\n");
#endif
	return 0;
}
