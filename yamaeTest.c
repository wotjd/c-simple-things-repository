#include <stdio.h>

const char *dataIn = "{\n\"Command\" : \"Command Value\",\n\"Action\" : \"Action Value\"\n}";

int main() {
	char v1[20] = {0,}, v2[20] = {0,}, v3[20] = {0,}, v4[20] = {0,};

	sscanf(dataIn, "%*[^\"]\" %[^\"]\" %*[^\"]\" %[^\"]\" %*[^\"]\" %[^\"]\" %*[^\"]\" %[^\"]", v1, v2, v3, v4);

	printf("dataIn : \n%s\n", dataIn);
	printf("parse result : %s, %s, %s, %s\n", v1, v2, v3, v4);

	char dataOut[512] = {0,};
	sprintf(dataOut, "{\n\"%s\" : \"%s\",\n\"%s\" : \"%s\"\n}", "command", "value1", "action", "value2");

	printf("\ndataOut : \n%s\n", dataOut);
	return 0;
}
