#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>

int main() {
	struct addrinfo* ailist;
	struct addrinfo hint;
	struct sockaddr_in* sinp;
	const char* addr;
	char abuf[INET_ADDRSTRLEN];

	hint.ai_flags = AI_CANONNAME;
	hint.ai_family = 0;
	hint.ai_socktype = 0;
	hint.ai_protocol = 0;
	hint.ai_addrlen = 0;
	hint.ai_canonname = NULL;
	hint.ai_addr = NULL;
	hint.ai_next = NULL;

	const char* host = "naver.com";

	if (getaddrinfo(host, NULL, &hint, &ailist)) {
		printf("error: getaddrinfo()\n");
		exit(1);
	}

	struct addrinfo* aip;
	for (aip = ailist; aip; aip = aip->ai_next) {
		char buf[512];
		if (getnameinfo(aip->ai_addr, sizeof(struct sockaddr), buf, 512, NULL, 0, 0) == 0)
			printf("%s : %s\n", aip->buf);
	}

	return 1;
}
