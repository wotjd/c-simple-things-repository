#include <stdio.h>
#include <stdbool.h>

unsigned char stopable = 0x00;

void password(char key) {
	static char pass = 1;
	
	switch (key - '0') {
		case 0 : pass = (pass != 4)? 0 : pass;
			break;
		case 2 : pass = (pass != 2 && pass != 3 && pass != 7)? 0 : pass;
			break;
		case 5 : pass = (pass != 8)? 0 : pass;
			break;
		case 6 : pass = (pass != 6)? 0 : pass;
			break;
		case 7 : pass = (pass != 1)? 0 : pass;
			break;
		case 8 : pass = (pass != 5)? 0 : pass;
			break;
		default : 
			pass = 0;
	}

	if (pass) {
		stopable |= (0x01 << pass -1);
		pass++;
	} else {
		stopable = 0x00;
		pass = 1;
	}
}

int main() {
	char key = 0;
	while ((key = getchar()) != 'q') {
		if (key != '\n') {
			password(key);
			printf("key : %c, stopSequence : %s, stopable = %02x\n", key, (stopSequence)? "true" : "false", stopable);
		}
	} 
	return 0;
}