#include <stdio.h>

void hihi(char* ptr) {
	printf("%x %x\n", ptr, *ptr);
}

int main() {
	char test[250] = {1, 2, 3};
	printf("%x %x\n", &test, test[0]);
	hihi(test);
	return 0;
}
