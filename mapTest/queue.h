#ifndef __QUEUE_H__
#define __QUEUE_H__

typedef struct _element {
	char *data;
	struct _element *next;
} Element;

typedef struct _queue {
	Element *front;
	Element *rear;
} Queue;

void put(Queue *, char *);
Element *get(Queue *);

#endif
