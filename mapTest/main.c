#include <stdio.h>
#include <stdlib.h>

#include "map.h"
#include "queue.h"

// iter test
int main() {
	map_str_t map;
	map_init(&map);
	
	char *string[5] = {"hello", ", ", "world", "!\n", NULL};

	for (int i = 0; i < 4; i++) {
		char intKey[20] = {0, };
		sprintf(intKey, "%d", i);
		map_set(&map, intKey, string[i]);
		printf("%s", string[i]);
	}

	const char *key;
	map_iter_t iter = map_iter(&map);

	while ((key = map_next(&map, &iter))) {
		printf("%s\n", *map_get(&map, key));
	}

	map_deinit(&map);	// free allocated memory.
	return 0;
}
