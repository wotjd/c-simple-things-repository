#include <stdio.h>

struct foo {
	int a[10];
#define end a[9]
};

int main() {
	struct foo b;
	int a[10];
	int i;
	for (i = 0; i < 10; i++) {
		b.a[i] = i + 10;
		a[i] = i;
	}

	printf("%d %d\n", end, b.end);
	return 0;
}
