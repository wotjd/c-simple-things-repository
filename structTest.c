#include <stdio.h>

typedef struct {
	int a;
	int b;
	int c;
	float f;
} Obj;

typedef struct {
	int a;
	int b;
	float f;
	int c;
} Obj1;

typedef struct {
	int a;
	int b;
	int c;
} Obj2;

void castingTest(Obj2 *obj) {
	printf("[castingTest] %d, %d, %d\n", obj->a, obj->b, obj->c);
}

int main() {
	Obj obj = {1,2,3,0.456};
	printf("[main] %d, %d, %d, %f\n", obj.a, obj.b, obj.c, obj.f);
	castingTest((Obj2 *)&obj);
	Obj1 obj1 = {1,2,0.345,6};
	printf("[main] %d, %d, %f, %d\n", obj1.a, obj1.b, obj1.f, obj1.c);
	castingTest((Obj2 *)&obj1);
	return 0;
}
