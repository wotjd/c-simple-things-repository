#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iconv.h>
#include <wchar.h>

int main() {
	char *url = "/home/wotjd/Downloads/Mario.png";
	char utfStr[128] = {0,};

	for (int i = 0, j = 0; j < sizeof(url); j++) {
		utfStr[i++] = *(url + j);
		utfStr[i++] = NULL;
	}

	iconv_t convertor = iconv_open("UTF-8", "UTF-16LE");
	if (convertor == (iconv_t)-1) {
		printf("failed to open iconv...\n");
	} else {
		printf("opened iconv.\n");
		char *input = &utfStr;
		int inputSize = 31 * sizeof(short);
		int outputSize = 32 * sizeof(short);
		int outputLeft = outputSize;
		char *output = (char *)calloc(sizeof(short), outputSize);
		char *result = output;

		printf("in %d out %d", inputSize, outputSize);
		int size = iconv(convertor, &input, &inputSize, &result, &outputLeft);
		
		printf("size : %d, left : %d\n",size, outputLeft);

		printf("converted : ");
		for (int i = 0; i < outputSize; i++) {
			printf("%c",*(output + i));
		}
		printf("\n");
		iconv_close(convertor);
	}

	return 0;
}
