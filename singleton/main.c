#include <stdio.h>
#include "Singleton.h"
#include "AnotherRoute.h"

int main() {
	printf("return from directly invoke : %d\n", getValue());
	printf("return from invoke anotherRoute : %d\n", getValueAnother());
	printf("return from directly invoke again : %d\n", getValue());
}
