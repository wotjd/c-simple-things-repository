#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include "CharacterList.h"

int main() {
	int i;
	for (i = 0; i < wcslen(characterList); i++) {
		wprintf("%c", (wchar_t)characterList[i]);
	}
	// printf("size : %d\n", wcslen(characterList));
	return 0;
}
