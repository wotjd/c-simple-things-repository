#include <stdio.h>

int main() {
	float a = 1.0;
	char *t = &a;
	printf("%02x %02x %02x %02x\n", t[0], t[1], t[2], t[3]);

	return 0;
}
