#include <stdio.h>

int main() {
	char *str = "jtbc3,233.15.220.172,5000,JTBC3,14.36.0.73,7175,0x1b,7176,0x81";

	char name[32] = {0,}, ip[16] = {0,}, port[6] = {0,}, dumpName[32] = {0, }, dumpIp[16] = {0,};
	short vpid = 0, apid = 0;
	int vtype = 0, atype = 0;

	printf("str : %s\n", str);

	if(sscanf(str, "%[^,], %[^,], %[^,], %[^,], %[^,], %hd, 0x%x, %hd, 0x%x", name, ip, port, dumpName, dumpIp, &vpid, &vtype, &apid, &atype) != 9)
		printf("syntax error\n");
	else
		printf("read name : %s, ip : %s, port : %s, dumpname : %s, dumpip : %s, vpid : %d, vtype : 0x%x, apid : %d, atype : 0x%x\n", name, ip, port, dumpName, dumpIp, vpid, vtype, apid, atype);
	
	return 1;
}
